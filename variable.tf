variable "do_token" {
    type = string
    description = "This is the token where you connect to DO"
}

variable "pub_key" {
    description = "This is the SSH public key where you place"
}

variable "pvt_key" {
    description = "This is the SSH private key where you place"
}

variable "instance_os" {
    type        = string
    description = "The OS used on your server instance"
}

variable "instance_name" {
    type        = string
    description = "The name define for your server instance"
}

variable "instance_region" {
    type        = string
    description = "The region define where to run your server instance"
}

variable "droplet_size" {
    type        = string
    description = "The size define for your server instance"
}

variable "git_username" {
    type        = string
    description = "The git username used for clone git project"
}

variable "git_password" {
    type        = string
    description = "The git password used for clone git project"
}

variable "open_weather_api_key" {
    type        = string
    description = "The API Key to call open Weather API"
}

variable "digitalocean_project_name" {
    type        = string
    description = "The project name to gather proposed application resources"
}

variable "digitalocean_project_description" {
    type        = string
    description = "The project description to elaborate proposed application"
}

variable "deploy_version" {
    type        = string
    default     = "20210922120000"
    description = "Deployment Version"
}


