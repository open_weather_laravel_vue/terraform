locals {

    digitalocean_project_name = var.digitalocean_project_name

    digitalocean_project_description = var.digitalocean_project_description

}

resource "digitalocean_project" "weather_app" {
    name        = local.digitalocean_project_name
    description = local.digitalocean_project_description
    purpose     = "Web Application"
    resources   = [digitalocean_droplet.weather_app[0].urn]
}