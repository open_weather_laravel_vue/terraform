
locals {

    open_weather_api_key = var.open_weather_api_key

}

data "template_file" "weather_app_env" {
    template = file("${path.module}/data/template/weather/.env")

    vars = {
        open_weather_api_key = "${local.open_weather_api_key}"
    }
}