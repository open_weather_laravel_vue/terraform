terraform {
    cloud {
        organization = "Nvnz"

        workspaces {
            name = "open-weather-laravel-vue"
        }
    }

    required_providers {
        digitalocean = {
            source = "digitalocean/digitalocean"
            version = "~> 2.0"
        }
    }
}

provider "digitalocean" {
    token = var.do_token
}