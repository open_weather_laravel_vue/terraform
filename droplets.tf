locals {

    instance_os = var.instance_os

    instance_name = var.instance_name

    instance_region = var.instance_region

    droplet_size = var.droplet_size

    git_username = var.git_username

    git_password = var.git_password

}

data "local_file" "public_key" {
    filename = "${path.module}/data/${var.pub_key}"
}

data "local_file" "private_key" {
    filename = "${path.module}/data/${var.pvt_key}"
}

resource "digitalocean_ssh_key" "terraform" {
    name       = "Open Weather Laravel Vue"
    public_key = data.local_file.public_key.content
}

data "template_file" "bash_script_setup" {
    template = file("${path.module}/data/scripts/setup.sh")

    vars = {
        git_username = "${local.git_username}"
        git_password = "${local.git_password}"
    }
}

resource "digitalocean_droplet" "weather_app" {
    count = 1
    image = "${local.instance_os}"
    name = "${local.instance_name}-${count.index}"
    region = "${local.instance_region}"
    size = "${local.droplet_size}"

    ssh_keys = [
        digitalocean_ssh_key.terraform.fingerprint
    ]

    connection {
        host        = self.ipv4_address
        type        = "ssh"
        user        = "root"
        private_key = data.local_file.private_key.content
    }

	provisioner "file" {
		source      = "${path.module}/data/vhost/000-default.conf"
		destination = "/tmp/000-default.conf"
	}

    provisioner "file" {
		content      = data.template_file.bash_script_setup.rendered
		destination = "/tmp/setup.sh"
	}

    provisioner "remote-exec" {
		inline = [
            "sudo apt install software-properties-common",
            "sudo apt-get update",
            "sudo apt-get install dos2unix",
		]
	}

    provisioner "remote-exec" {
		inline = [
			"dos2unix /tmp/setup.sh",
            "sudo chmod +x /tmp/setup.sh",
			"sudo /tmp/setup.sh",
		]
	}


    droplet_agent     = true
    graceful_shutdown = true

	# lifecycle {
	# 	prevent_destroy = true
	# }
}

resource "null_resource" "weather_app_update" {
	triggers = {
		version = var.deploy_version
	}

    connection {
        host        = digitalocean_droplet.weather_app[0].ipv4_address
        type        = "ssh"
        user        = "root"
        private_key = data.local_file.private_key.content
    }

    provisioner "file" {
		content      = data.template_file.weather_app_env.rendered
		destination = "/tmp/.env"
	}

	provisioner "file" {
		source      = "${path.module}/data/scripts/update.sh"
		destination = "/tmp/update.sh"
	}

	provisioner "remote-exec" {
		inline = [
            "dos2unix /tmp/update.sh",
			"sudo chmod +x /tmp/update.sh",
			"sudo /tmp/update.sh",
		]
	}
}

