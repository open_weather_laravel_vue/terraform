#!/bin/bash

####
sudo apt install git -y
echo -e '\n---- END Install Git ----\n'

####
sudo apt-get install apache2 -y
echo -e '\n---- END Install Apache ----\n'

####
sudo add-apt-repository ppa:ondrej/php -y
sudo apt -y install php7.4
sudo apt-get install -y php7.4-cli php7.4-json php7.4-common php7.4-mysql php7.4-zip php7.4-gd php7.4-mbstring php7.4-curl php7.4-xml php7.4-bcmath
echo -e '\n---- END Install Php 7.4 ----\n'

####
sudo apt install unzip
curl -sS https://getcomposer.org/installer -o /tmp/composer-setup.php
HASH=`curl -sS https://composer.github.io/installer.sig`
php -r "if (hash_file('SHA384', '/tmp/composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
sudo php /tmp/composer-setup.php --install-dir=/usr/local/bin --filename=composer
sudo composer self-update --1
echo -e '\n---- END Install Composer ----\n'

curl -sL https://deb.nodesource.com/setup_16.x -o /tmp/nodesource_setup.sh
sudo bash /tmp/nodesource_setup.sh
sudo apt install nodejs -y
echo -e '\n---- END Install Nodejs ----\n'

####
cd /var/www/html/
sudo git clone https://${git_username}:${git_password}@gitlab.com/open_weather_laravel_vue/laravel-vue.git
echo -e '\n---- END Git Clone ----\n'

####
APACHE_CONFIG_FILE='/etc/apache2/sites-available/000-default.conf'
sudo cp -f /tmp/000-default.conf "$APACHE2_CONFIG_FILE"
sudo a2enmod rewrite
sudo systemctl restart apache2
echo -e '\n---- END Apache Config ----\n'