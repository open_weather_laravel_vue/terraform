#!/bin/bash

PROJECT_PATH='/var/www/html/laravel-vue'
PROJECT_ENV='/var/www/html/laravel-vue/.env'

####
cd "$PROJECT_PATH"
sudo git pull
echo -e '\n---- END Git Pull ----\n'

####
sudo chown -R www-data:www-data "$PROJECT_PATH"
echo -e '\n---- END Change Own Mode to www-data ----\n'

####
sudo cp -f /tmp/.env "$PROJECT_ENV"
sudo composer install
sudo php artisan optimize:clear
echo -e '\n---- END Run Composer install command ----\n'

sudo php artisan key:generate
echo -e '\n---- END Generate Laravel key ----\n'

sudo npm install
sudo npm run prod
echo -e '\n---- END Project Setup ----\n'

